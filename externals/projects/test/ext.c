#include "m_pd.h"

static t_class *ext_class;

typedef struct _ext{
	t_object  x_obj;
	int counter, lower, upper;
	t_float step;
	t_outlet *o1f, *o2b;

} t_ext;

void ext_bang(t_ext *x){
	t_float f=x->counter;
	int step = x->step;
	x->counter+=step;
	if (x->lower-x->upper){
		if ((step>0) && (x->counter > x->upper)) {
			x->counter = x->lower;
			outlet_bang(x->o2b);
		} else if (x->counter < x->lower) {
			x->counter = x->upper;
			outlet_bang(x->o2b);
		}
	}
	outlet_float(x->o1f, f);
}

void ext_reset(t_ext *x){
	x->counter = x->lower;
}

void ext_set(t_ext *x, t_floatarg f){
	x->counter = f;
}

void ext_bound(t_ext *x, t_floatarg f1, t_floatarg f2){
	x->lower = (f1<f2)?f1:f2;
	x->upper = (f1>f2)?f1:f2;
}


void *ext_new(t_symbol *s, int argc, t_atom *argv){
	t_ext *x = (t_ext *)pd_new(ext_class);


	t_float f1=0, f2=0;

	x->step=1;
	switch(argc){
	default:
	case 3:
	  x->step=atom_getfloat(argv+2);
	case 2:
	  f2=atom_getfloat(argv+1);
	case 1:
	  f1=atom_getfloat(argv);
	  break;
	case 0:
	  break;
	}
	if (argc<2)f2=f1;
	x->lower = (f1<f2)?f1:f2;
	x->upper   = (f1>f2)?f1:f2;

	x->counter=x->lower;

	inlet_new(&x->x_obj, &x->x_obj.ob_pd,gensym("list"), gensym("bound"));
	floatinlet_new(&x->x_obj, &x->step);
	x->o1f=outlet_new(&x->x_obj, &s_float);
	x->o2b=outlet_new(&x->x_obj, &s_bang);

	return (void *)x;
}

// void *ext_new(t_floatarg extcounter_value){
// 	t_ext *x = (t_ext *)pd_new(ext_class);

// 	x->counter=extcounter_value;
// 	outlet_new(&x->x_obj,&s_float);

// 	return (void *)x;
// }

void ext_setup(void){
	ext_class = class_new(gensym("ext"),
		(t_newmethod)ext_new,
		0, sizeof(t_ext),
		CLASS_DEFAULT,
		A_GIMME, 0);

	class_addbang(ext_class, ext_bang);
	class_addmethod(ext_class,(t_method)ext_reset,gensym("reset"),0);
	class_addmethod(ext_class,(t_method)ext_set,  gensym("set"),A_DEFFLOAT,0);
	class_addmethod(ext_class,(t_method)ext_bound,gensym("bound"),A_DEFFLOAT,A_DEFFLOAT,0);
}
