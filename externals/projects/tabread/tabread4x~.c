/* Copyright (c) 1997-1999 Miller Puckette and others.
* For information on usage and redistribution, and for a DISCLAIMER OF ALL
* WARRANTIES, see the file, "LICENSE.txt," in this distribution.  */

/* sampling */

/* LATER make tabread4X and tabread~ */

#include "m_pd.h"




/******************** tabread4X~ ***********************/

static t_class *tabread4X_tilde_class;

typedef struct _tabread4X_tilde
{
    t_object x_obj;
    int x_npoints;
    t_word *x_vec;
    t_symbol *x_arrayname;
    t_float x_f;
    t_float x_onset;
} t_tabread4X_tilde;

static void *tabread4X_tilde_new(t_symbol *s)
{
    t_tabread4X_tilde *x = (t_tabread4X_tilde *)pd_new(tabread4X_tilde_class);
    x->x_arrayname = s;
    x->x_vec = 0;
    outlet_new(&x->x_obj, gensym("signal"));
    floatinlet_new(&x->x_obj, &x->x_onset);
    x->x_f = 0;
    x->x_onset = 0;
    return (x);
}

static t_int *tabread4X_tilde_perform(t_int *w)
{
    t_tabread4X_tilde *x = (t_tabread4X_tilde *)(w[1]);
    t_sample *in = (t_sample *)(w[2]);
    t_sample *out = (t_sample *)(w[3]);
    int n = (int)(w[4]);
    int maxindex;
    t_word *buf = x->x_vec, *wp;
    double onset = x->x_onset;
    int i;

    maxindex = x->x_npoints - 3;
    if(maxindex<0) goto zero;

    if (!buf) goto zero;

#if 0       /* test for spam -- I'm not ready to deal with this */
    for (i = 0,  xmax = 0, xmin = maxindex,  fp = in1; i < n; i++,  fp++)
    {
        t_sample f = *in1;
        if (f < xmin) xmin = f;
        else if (f > xmax) xmax = f;
    }
    if (xmax < xmin + x->c_maxextent) xmax = xmin + x->c_maxextent;
    for (i = 0, splitlo = xmin+ x->c_maxextent, splithi = xmax - x->c_maxextent,
        fp = in1; i < n; i++,  fp++)
    {
        t_sample f = *in1;
        if (f > splitlo && f < splithi) goto zero;
    }
#endif

    for (i = 0; i < n; i++)
    {
        double findex = *in++ + onset;
        int index = findex;
        t_sample frac,  a,  b,  c,  d, cminusb;
        if (index < 1)
            index = 1, frac = 0;
        else if (index > maxindex)
            index = maxindex, frac = 1;
        else frac = findex - index;
        wp = buf + index;
        a = wp[-1].w_float;
        b = wp[0].w_float;
        c = wp[1].w_float;
        d = wp[2].w_float;
        cminusb = c-b;
        *out++ = b + frac * (
            cminusb - 0.1666667f * (1.-frac) * (
                (d - a - 3.0f * cminusb) * frac + (d + 2.0f*a - 3.0f*b)
            )
        );
    }
    return (w+5);
 zero:
    while (n--) *out++ = 0;

    return (w+5);
}

static void tabread4X_tilde_set(t_tabread4X_tilde *x, t_symbol *s)
{
    t_garray *a;

    x->x_arrayname = s;
    if (!(a = (t_garray *)pd_findbyclass(x->x_arrayname, garray_class)))
    {
        if (*s->s_name)
            pd_error(x, "tabread4X~: %s: no such array", x->x_arrayname->s_name);
        x->x_vec = 0;
    }
    else if (!garray_getfloatwords(a, &x->x_npoints, &x->x_vec))
    {
        pd_error(x, "%s: bad template for tabread4X~", x->x_arrayname->s_name);
        x->x_vec = 0;
    }
    else garray_usedindsp(a);
}

static void tabread4X_tilde_dsp(t_tabread4X_tilde *x, t_signal **sp)
{
    tabread4X_tilde_set(x, x->x_arrayname);

    dsp_add(tabread4X_tilde_perform, 4, x,
        sp[0]->s_vec, sp[1]->s_vec, sp[0]->s_n);

}

static void tabread4X_tilde_free(t_tabread4X_tilde *x)
{
}

static void tabread4X_tilde_setup(void)
{
    tabread4X_tilde_class = class_new(gensym("tabread4X~"),
        (t_newmethod)tabread4X_tilde_new, (t_method)tabread4X_tilde_free,
        sizeof(t_tabread4X_tilde), 0, A_DEFSYM, 0);
    CLASS_MAINSIGNALIN(tabread4X_tilde_class, t_tabread4X_tilde, x_f);
    class_addmethod(tabread4X_tilde_class, (t_method)tabread4X_tilde_dsp,
        gensym("dsp"), A_CANT, 0);
    class_addmethod(tabread4X_tilde_class, (t_method)tabread4X_tilde_set,
        gensym("set"), A_SYMBOL, 0);
}
