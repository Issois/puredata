#%% import
# import numpy
# import matplotlib
# import pandas
# import glob
import pathlib
# import configparser
import os
# import re
from shutil import copyfile
#%%
SCRIPTPATH=str(pathlib.Path(__file__).parent.absolute())

#%%
ROOT=os.path.join(SCRIPTPATH,"..","patches")

flattened=os.path.join(SCRIPTPATH,"..","flattened")
os.mkdir(flattened)

for root,folder,files in os.walk(ROOT):
	for file in files:
		if file[-2:]=="pd":
			copyfile(os.path.join(root,file),os.path.join(flattened,file))
