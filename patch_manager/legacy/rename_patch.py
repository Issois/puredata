#%% import
# import numpy
# import matplotlib
# import pandas
# import glob
import sys
import re
import pathlib
import os

#%%
head="rename_patch.py: "
SCRIPTPATH=str(pathlib.Path(__file__).parent.absolute())
ROOT=os.path.join(SCRIPTPATH,"..","patches")
args=sys.argv
if not len(args)==3:
	print(head+"Need two argument. ("+str(args)+")")
	sys.exit(1)

oldname=args[1]
newname=args[2]

# print("root: "+args[0])

print(head+"Renaming patch \""+oldname+"\" to \""+newname+"\" and updating all references.")

for root,folder,files in os.walk(ROOT):
# 	print(" "+str(folder)+" "+str(files))
	for file in files:
# 		print(file+" <-> "+oldname+".py")
		if file==(oldname+".pd"):
			os.rename(os.path.join(root,file),os.path.join(root,newname+".pd"))
			print(head+"Renamed file \""+file+"\" to \""+newname+".pd\".")
		else:
# 			print(head+"Searching in file \""+file+"\" for occurrences of object \""+oldname+"\" to replace with \""+newname+"\".")
			with open(os.path.join(root,file),"r+") as f:
				source=f.read()
				pattern=r"(#X obj \d+ \d+ )("+oldname+")( |;)"
				repl=r"\1"+newname+r"\3"
				result,replacecount=re.subn(pattern=pattern,repl=repl,string=source,flags=re.MULTILINE)
				if replacecount>0:
					print(head+"Replaced "+str(replacecount)+" occurrences of \""+oldname+"\" with \""+newname+"\" in file \""+file+"\".")
					f.seek(0)
					f.write(result)
					f.truncate()


sys.exit(0)
