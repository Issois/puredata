#%%
import os
import glob
import re
import pathlib
#%%
path=str(pathlib.Path(__file__).parent.absolute())

#%%
files=glob.glob(path+"\\*.pd")

content={}
# rgx=re.compile(".*canvas")

for filename in files:
	with open(filename, 'r') as file:
		content[filename]= file.read().replace('\n', '')

del file,filename
#%%
patchnames=[os.path.splitext(os.path.basename(name))[0] for name in glob.glob(path+"\\*.pd")]
pattern=["("+name+")" for name in patchnames]
pattern="|".join(pattern)
pattern=r"#X obj \d+ \d+ ("+pattern+")(;| )"
#X obj 9 13 memory_wrapper
# pattern=".*("+("|".join(patchNames))+")"
rgx=re.compile(pattern)
del pattern
#%%

refs={}
finds={}

for filename in content:
	refs[filename]=[]
# 	match=rgx.match(content[filename])
# 	finds=rgx.findall(content[r"D:\_\30_Git\Issois\puredata\patches\first_loop.pd"])
	finds[filename]=rgx.findall(content[filename])
	for tup in finds[filename]:
		for elem in tup:
			if len(elem)>0:
				if elem not in refs[filename]:
					refs[filename].append(elem)

del tup,elem,filename

#%%
allrefs=[]
for ref in refs:
	for elem in refs[ref]:
		if elem not in allrefs:
			allrefs.append(elem)

del ref,elem

#%%

nonreffed=[]
for name in patchnames:
	if name not in allrefs:
		nonreffed.append(name)

del name