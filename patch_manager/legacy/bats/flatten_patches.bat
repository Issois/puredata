@echo off

set source=%~dp0patches\
set destination=%~dp0flattened

mkdir %destination%

for /r %source% %%F in (.) do if "%%~fF" neq %destination% ROBOCOPY "%%F" %destination%